
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../model/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  @Input() title: string;
  @Input() todoList: Todo[];

  @Output() newTodoAdded: EventEmitter<string> = new EventEmitter();
  @Output() todoMarkedAsDone: EventEmitter<string> = new EventEmitter();

  newTodo: string;
  constructor() {
    this.newTodo = '';
   }

  ngOnInit() {
    // console.log(this.title);
    console.log(this.todoList);
  }

  onEnter() {
    console.log(this.newTodo);
    console.log('Enter key pressed');
    this.newTodoAdded.emit(this.newTodo);
    // this.todoList.push(this.newTodo);
    this.newTodo = '';
  }

  ngOnCheck() {

  }

  onChecked(createdDate: string) {
    this.todoMarkedAsDone.emit(createdDate);
  }

  ngOnDestroy(){
    // Garbage collection
    //   
  }

}
