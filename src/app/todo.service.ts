import { Injectable } from '@angular/core';
import { Todo } from './model/todo';

@Injectable()
export class TodoService {

  private projectTodoList: Todo[] = [];
  private personalTodoList: Todo[] = [];

  constructor() { 
    this.projectTodoList = JSON.parse(localStorage.getItem('projectTodos')) || [];
    this.personalTodoList = JSON.parse(localStorage.getItem('personalTodos')) || [];
  }

  deleteTodo(createdDate: string, type: string) {
    const dateCreated = parseInt(createdDate);
    if (type === 'project') {
      this.deleteItemFromList(this.projectTodoList, dateCreated);
      localStorage.setItem('projectTodos', JSON.stringify(this.projectTodoList));
      
    } else {
      this.deleteItemFromList(this.personalTodoList, dateCreated);
      localStorage.setItem('personalTodos', JSON.stringify(this.personalTodoList));
      
    }
  }

  deleteItemFromList(list: Todo[], createdDate: number) {
    const index = list.findIndex((todo: Todo) => todo.createdDate === createdDate);
    list.splice(index, 1);
  }

  get projectTodos() {
    return this.projectTodoList;
  }

  get personalTodos() {
    return this.personalTodoList;
  }

  set projectTodo(todos: Todo[]) {
    if (todos.length === 0) {
      console.log('Cannot set empty todo list');
      return;
    }
    this.projectTodoList = todos;
  }

  addNewTodo(todo: Todo, type: string) {
    if (type === 'project') {
      this.projectTodoList.push(todo);
      localStorage.setItem('projectTodos', JSON.stringify(this.projectTodoList));
    } else {
      this.personalTodoList.push(todo);
      localStorage.setItem('personalTodos', JSON.stringify(this.personalTodoList));
    }
  }

}
