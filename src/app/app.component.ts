import { Component } from '@angular/core';
import { Todo } from './model/todo';
import { TodoService } from './todo.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ TodoService ]
})
export class AppComponent {
  title = 'app';
  projectTodoList: Todo[] = [];
  personalTodoList: Todo[] = [];

  constructor(private todoService: TodoService) {
    this.projectTodoList = todoService.projectTodos;
    this.personalTodoList = todoService.personalTodos;
  }

  onNewItemAdded(newTaskName: string, type: string) {
    this.todoService.addNewTodo(new Todo(newTaskName, Date.now()), type);
  }

  onTodoMarkedAsDone(createdDate: string, type: string) {
    this.todoService.deleteTodo(createdDate, type);
  }
}
