import { Directive, Input, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appStatusHighlighter]'
})
export class StatusHighlighterDirective {

  @Input() createdTimeStamp: number;
  
  constructor(private el: ElementRef) { }

  
// @HostListener('mouseenter') onmouseenter() {
//   console.log('mouse entered');
//   let classToBeApplied = '';
//   switch(this.findElapsedDays(this.createdTimeStamp)) {
//     case 0:
//     classToBeApplied = 'status-green';
//     break;
//     case 1:
//     classToBeApplied = 'status-orange';
//     break;
//     case 2:
//     classToBeApplied = 'status-red';
//     break;
//   }
//     this.el.nativeElement.className += ' ' + classToBeApplied;
// }

// @HostListener('mouseout') onMouseOut() {
//   this.el.nativeElement.className = 'status-marker';
// } 

  ngOnInit() {
    // console.log(this.createdTimeStamp);
    // console.log(this.el.nativeElement);
    let classToBeApplied = '';
    switch(this.findElapsedDays(this.createdTimeStamp)) {
      case 0:
      classToBeApplied = 'status-green';
      break;
      case 1:
      classToBeApplied = 'status-orange';
      break;
      case 2:
      classToBeApplied = 'status-red';
      break;
    }
    this.el.nativeElement.className += ' ' + classToBeApplied;
  }

  // Return 0 if same day
  // Return 1 if yesterday
  // Return 2 if more than 2 days old

  findElapsedDays(todoCreatedDate: number) {
    const today = Date.now();
    const diffInMilliSeconds = today - todoCreatedDate;
    const milliSecondsInADay = 60 * 1000;
    const milliSecondsIn2Days = 2 * milliSecondsInADay;

    if (diffInMilliSeconds <= milliSecondsInADay) {
      return 0;
    } else if (diffInMilliSeconds > milliSecondsInADay && diffInMilliSeconds <= milliSecondsIn2Days) {
      return 1;
    } else {
      return 2;
    }
  }

}
