export class Todo {
    name: string;
    createdDate: number;

    constructor(todoName: string, todoCreatedDate: number) {
        this.name = todoName;
        this.createdDate = todoCreatedDate;
    }
}